import boto3.session
import datetime
import json
import logging
import logging.config
import multiprocessing
import os
import psycopg2
import redis
import statsd
from minty.config import parser
from smart_open import open
from typing import TYPE_CHECKING, Literal, Union

PREFIX_LENGTH = len("saas:instance:")

if TYPE_CHECKING:
    TYPE_QUEUE = multiprocessing.Queue[Union[Literal["STOP"], tuple[str, str]]]
else:
    TYPE_QUEUE = multiprocessing.Queue

REDIS_URL = os.environ["REDIS_URL"]
S3_BUCKET = os.environ["S3_BUCKET"]
STATSD_HOST = os.environ.get("STATSD_HOST", "localhost")
STATSD_PORT = int(os.environ.get("STATSD_PORT", "8125"))
PARALLEL_COUNT = int(os.environ.get("PARALLEL_EXPORT_COUNT", "5"))
PAGE_SIZE = int(os.environ.get("PAGE_SIZE", "250"))

statsd.Connection.set_defaults(host=STATSD_HOST, port=STATSD_PORT)
stats_timer = statsd.Timer("bidumper")
stats_counter = statsd.Counter("bidumper")

RUN_TIMESTAMP = datetime.datetime.now(tz=datetime.timezone.utc).strftime(
    "%Y-%m-%d"
)


if log_config := os.environ.get("LOG_CONFIG_FILE", None):
    logging.config.fileConfig(log_config)
else:
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": True,
            "formatters": {
                "standard": {
                    "format": "%(asctime)s %(levelname)s %(name)s: %(message)s"
                },
            },
            "handlers": {
                "default": {
                    "level": "NOTSET",
                    "formatter": "standard",
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stderr",  # Default is stderr
                },
            },
            "loggers": {
                "": {  # root logger
                    "handlers": ["default"],
                    "level": "DEBUG",
                    "propagate": False,
                },
                "__main__": {  # if __name__ == '__main__'
                    "handlers": ["default"],
                    "level": "DEBUG",
                    "propagate": False,
                },
            },
        }
    )

logger = logging.getLogger(__name__)

session = boto3.session.Session()

# 2021-09 boto3 has no way to override this from config/environment
# Follow https://github.com/boto/boto3/pull/2746
if s3_endpoint := os.environ.get("S3_ENDPOINT"):
    S3_CLIENT = session.client("s3", endpoint_url=s3_endpoint)
else:
    S3_CLIENT = session.client("s3")


@stats_timer.decorate
def make_dump(s3_filename_base: str, cursor):
    cursor.execute("SELECT MIN(id), MAX(id) FROM zaak WHERE deleted IS NULL")
    (case_id_min, case_id_max) = cursor.fetchone()

    # The output of this program is one big JSON file containing all cases
    # in an array.
    # But we _don't_ want to read everything into several gigabytes of
    # RAM before we save it to S3.
    # To make that work, we write the outer "array" part ourselves, and
    # fill it with comma-separated objects -- making it a valid JSON array.

    with open(
        f"s3://{S3_BUCKET}/{s3_filename_base}-cases.json.gz",
        "wb",
        transport_params={"client": S3_CLIENT},
    ) as fh:  # type: ignore
        fh.write(b"[")

        if case_id_min is not None:  # "There are cases in this env at all"
            is_first_batch = True
            for start_case_id in range(
                case_id_min, case_id_max + 1, PAGE_SIZE
            ):
                cases = []
                end_case_id = min(start_case_id + PAGE_SIZE, case_id_max + 1)
                logger.debug(
                    f"Writing cases from {start_case_id} to {end_case_id}."
                )
                try:
                    cursor.execute(
                        """
                        SELECT json_build_object(
                            'type', 'case',
                            'reference', id,
                            'instance', row_to_json(case_v1)
                        ) from case_v1
                        WHERE number >= %(case_id_min)s AND number < %(case_id_max)s
                        ORDER BY id
                        """,
                        {
                            "case_id_min": start_case_id,
                            "case_id_max": end_case_id,
                        },
                    )

                    for row in cursor.fetchall():
                        cases.append(json.dumps(row[0]))

                    logger.debug(
                        f"Finished cases from {start_case_id} to {end_case_id}."
                    )
                except (Exception, psycopg2.DatabaseError):
                    cursor.execute("rollback")
                    current_id = start_case_id

                    while current_id <= end_case_id:
                        try:
                            cursor.execute(
                                """
                                SELECT json_build_object(
                                'type', 'case',
                                'reference', id,
                                'instance', row_to_json(case_v1)
                                ) from case_v1
                                WHERE number = %(case_id_min)s
                                """,
                                {
                                    "case_id_min": current_id,
                                },
                            )
                            case = cursor.fetchone()
                            if case:
                                cases.append(json.dumps(case[0]))
                                logger.debug(f"Finished case {current_id}.")
                        except (Exception, psycopg2.DatabaseError) as error:
                            reference = None
                            cursor.execute(
                                """SELECT uuid FROM zaak WHERE id = %(case_id)s """,
                                {
                                    "case_id": current_id,
                                },
                            )

                            row = cursor.fetchone()
                            if row:
                                reference = row[0]

                            error_message = {
                                "type": "case",
                                "reference": reference,
                                "error": f"Case {current_id} is broken : {error}"
                            }
                            cases.append(json.dumps(error_message))
                            logger.debug(f"Case {current_id} is broken.")
                        finally:
                            current_id = current_id + 1

                case_batch = ",".join(cases)
                if case_batch != "":
                    # Ensure we write [{},{}}] instead of [,{},{}] or [{},{},]
                    if not is_first_batch:
                        fh.write(b",")
                    elif is_first_batch:
                        is_first_batch = False

                    fh.write(case_batch.encode("utf-8"))

        fh.write(b"]")


def process_instance(instance_name: str, database_url: str) -> None:
    logger.info(f"Processing dump for instance={instance_name}")

    stats_counter.increment("process_instance")
    connection = psycopg2.connect(database_url)

    for _ in (True,):
        with connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT TRUE FROM interface
                WHERE module = 'bidownload'
                AND active IS TRUE
                AND date_deleted IS NULL
                """
            )
            config_value = cursor.fetchone()

            if not (config_value and config_value[0]):
                logger.info(
                    f"Skipping instance={instance_name}: BI integration not configured"
                )
                stats_counter.increment("skip_instance_unconfigured")
                break

            s3_filename_base = f"{instance_name}-{RUN_TIMESTAMP}"

            logger.info(f"Making dump for instance={instance_name}")
            make_dump(s3_filename_base, cursor)
            logger.info(f"Done with dump for instance={instance_name}")

    connection.close()
    return


def worker(process_num: int, input: TYPE_QUEUE) -> None:
    logger.debug(f"Starting worker {process_num}.")

    while True:
        next_input = input.get()
        if next_input == "STOP":
            logger.debug("Received STOP sentinel; ending run.")
            break
        else:
            instance_name, database_url = next_input

        process_instance(instance_name, database_url)

    logger.debug("Exiting worker.")


if __name__ == "__main__":
    redis_conn = redis.Redis.from_url(REDIS_URL)
    config_parser = parser.ApacheConfigParser()

    task_queue: TYPE_QUEUE = multiprocessing.Queue()

    for process_number in range(PARALLEL_COUNT):
        multiprocessing.Process(
            target=worker, args=(process_number, task_queue)
        ).start()

    for instance_name in redis_conn.smembers("saas:instances"):
        instance_name_str = instance_name.decode("utf-8")

        config_raw = redis_conn.get(f"saas:instance:{instance_name_str}")

        if config_raw is None:
            logger.warning(
                f"Empty configuration found for instance '{instance_name_str}'"
            )
            continue

        config = config_parser.parse(config_raw.decode("utf-8"))
        database_url = config["instance"]["zaaksysteemdb_ro.url"]
        instance_id = config["instance"]["logging_id"]

        task_queue.put((instance_id, database_url))

    for i in range(PARALLEL_COUNT):
        task_queue.put("STOP")
