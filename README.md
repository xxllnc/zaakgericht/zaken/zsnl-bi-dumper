# Zaaksysteem (BI) Dumper

This tool gets the list of customer databases, then for each one of them,
check if the "Needs a BI dump" option is enabled. If it is, it makes a dump (as
gzip'ed JSON), and saves that to S3.

The list of customer databases is retrieved from Redis on startup using a
"SCAN" on all keys matching "saas:instance:\*"

## Configuration

To configure the dumper tool, the following environment variables can be used:

* `REDIS_URL` - URL pointing to Redis that contains instance configurations.
  See <https://www.iana.org/assignments/uri-schemes/prov/redis> for format
  of these URLs (or check `docker-compose.yml`)

* `S3_ENDPOINT` - Sets the host to use for S3 requests; if not specified,
  defaults to whatever boto3 defaults to.

  All other S3 configuration is handled through `boto3` (using `~/.aws/config`):
  <https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#using-a-configuration-file>

* `S3_BUCKET` - Name of the bucket to store dumps in

* `LOG_CONFIG_FILE` - Name of the logging configuration file. See
  <https://docs.python.org/3/library/logging.config.html#logging-config-fileformat>
  for more information.
  The container is built with [python-json-logger](https://github.com/madzak/python-json-logger) preinstalled so JSON-structured log output can be configured.

* `STATSD_HOST` - Hostname where statsd stats should be sent; defaults to `localhost`

* `STATSD_PORT` - Port to send statsd stats to; defaults to `8125`

* `PARALLEL_EXPORT_COUNT` - Number of parallel workers to run; defaults to `5`

* `PAGE_SIZE` - Number of cases to retrieve per "page"; defaults to `250`

## Testing

To test the script, this repository contains a docker-compose setup:

```shell
host:$ docker-compose pull
[docker-compose output]

host:$ docker-compose up -d

# Load the test data into redis/postgresql
host:$ docker-compose run --rm dumper
container:$ python3 test_setup.py

# Run the test script
container:$ python3 dumper.py
```

You can now open <http://localhost:9001> to log in to the Minio console and
check the generated files.
